<?php

namespace App\Models;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use DB;


class Users extends Model
{
    public static function signUp($data)
    {
        $user = DB::insert('INSERT INTO user (id_profile, name, email, pass) VALUES (?, ?, ?, ?)', [2, $data->name, $data->email, Hash::make($data->password)]);
        if($user) {
            return array("success" => "OK", "data" => $user);
        } else {
            return array("success" => "ERROR", "data" => $user);
        }
    }

    public static function signIn($data)
    {
        $user = DB::select("SELECT id, pass, id_profile FROM user WHERE email='{$data->email}'");
        if($user) {
            if(Hash::check($data->password, $user[0]->pass)) {
                return array("success" => "OK", "data" => array("idUser" => $user[0]->id, "idProfile" => $user[0]->id_profile));
            } else {
                return array("success" => "ERROR", "data" => "Error de contraseña."); 
            }
        } else {
            return array("success" => "ERROR", "data" => "Usuario no registrado.");
        }
    }

    public static function users()
    {
        $users = DB::select("SELECT id, name FROM user WHERE id_profile=2");
        return array("success" => "OK", "data" => $users);            
    }
}
