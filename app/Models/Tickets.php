<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    public static function tickets()
    {
        $data = DB::select("SELECT t.id, u.name, t.status  FROM ticket t JOIN user u ON t.id_user = u.id");
        return array("success" => "OK", "data" => $data);
    }

    public static function ticketsbyuser($idUser)
    {
        $data = DB::select("SELECT t.id, u.name, t.status  FROM ticket t JOIN user u ON t.id_user = u.id WHERE t.id_user = {$idUser}");
        return array("success" => "OK", "data" => $data);
    }

    public static function ticket($id)
    {
        $data = DB::select("SELECT t.id, u.name, t.status, u.id id_user  FROM ticket t JOIN user u ON t.id_user = u.id WHERE t.id = {$id}");
        return array("success" => "OK", "data" => $data[0]);
    }

    public static function ticketnew($idUser)
    {
        $data = DB::insert("INSERT INTO ticket (id_user) VALUES (?)", [$idUser]);
        return array("success" => "OK", "data" => $data);
    }

    public static function ticketdel($idTicket)
    {
        DB::table('ticket')->where('id', $idTicket)->delete();
        $data = DB::select("SELECT t.id, u.name, t.status  FROM ticket t JOIN user u ON t.id_user = u.id");
        return array("success" => "OK", "data" => $data);
    }

    public static function ticketedit($data)
    {
        DB::table('ticket')->where('id', $data->idTicket)->update([
            "id_user" => $data->id,
            "status" => 0
        ]);
        $data = DB::select("SELECT t.id, u.name, t.status  FROM ticket t JOIN user u ON t.id_user = u.id");
        return array("success" => "OK", "data" => $data);
    }

    public static function ticketclaim($idTicket, $idUser)
    {
        DB::table('ticket')->where('id', $idTicket)->update([
            "status" => 1
        ]);
        $data = DB::select("SELECT t.id, u.name, t.status  FROM ticket t JOIN user u ON t.id_user = u.id WHERE t.id_user = {$idUser}");
        return array("success" => "OK", "data" => $data);
    }


}
