<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;

class UsersController extends Controller
{
    public function signUp(Request $request)
    {
        $response = Users::signUp($request);
        return json_encode($response);
    }

    public function signIn(Request $request)
    {
        $response = Users::signIn($request);
        return json_encode($response);
    }

    public function users()
    {
        $response = Users::users();
        return json_encode($response);
    }
}
