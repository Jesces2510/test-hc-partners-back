<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tickets;

class TicketsController extends Controller
{
    public function tickets()
    {
        $response = Tickets::tickets();
        return json_encode($response);
    }

    public function ticketnew(Request $request)
    {
        $response = Tickets::ticketnew($request->id);
        return json_encode($response);
    }

    public function ticketedit(Request $request)
    {
        $response = Tickets::ticketedit($request);
        return json_encode($response);
    }

    public function ticketdel(Request $request)
    {
        $response = Tickets::ticketdel($request->id);
        return json_encode($response);
    }

    public function ticket(Request $request)
    {
        $response = Tickets::ticket($request->id);
        return json_encode($response);
    }

    public function ticketsbyuser(Request $request)
    {
        $response = Tickets::ticketsbyuser($request->id);
        return json_encode($response);
    }

    public function ticketclaim(Request $request)
    {
        $response = Tickets::ticketclaim($request->id, $request->idUser);
        return json_encode($response);
    }
}
