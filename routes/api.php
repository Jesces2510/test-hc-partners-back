<?php

use Illuminate\Http\Request;

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400'); 
}

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Users

Route::post('/signup', 'UsersController@signUp');
Route::post('/signin', 'UsersController@signIn');
Route::get('/users', 'UsersController@users');

// Tickets

Route::get('/tickets', 'TicketsController@tickets');
Route::get('/ticketsbyuser/{id}', 'TicketsController@ticketsbyuser');
Route::post('/ticketnew', 'TicketsController@ticketnew');
Route::post('/ticketdel', 'TicketsController@ticketdel');
Route::post('/ticketedit', 'TicketsController@ticketedit');
Route::get('/ticket/{id}', 'TicketsController@ticket');
Route::post('/ticketclaim', 'TicketsController@ticketclaim');
